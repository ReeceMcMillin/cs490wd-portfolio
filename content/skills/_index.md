+++
title = "Skills"
sort_by = "date"
template = "blog.html"
+++

As much as I'd love to ramble about the nuances of [composing experimental neoclassical music](https://www.bandcamp.com/aerocity), roasting award-winning coffee, or cutting aluminum rail for garage doors, I'm limiting this page to *career-relevant* skills.

<div class="skills">

<fieldset class="skill">
<legend>Programming Languages</legend>

{% skill(name="Rust") %}

I use Rust for its strengths in domain modelling and, maybe more importantly, developer ergonomics. Beyond the usual promise of memory safety, Rust's strong type system helped me develop an intuition for <span class="highlight">type-driven development</span>.

I've developed tools like a strongly-typed [Rundeck](https://www.rundeck.com/) SDK, static site generators, RPC systems, web services, and more.

{% end %}


{% skill(name="Python") %}

Python has been the backbone of my professional work. In my day-to-day work, I primarily use Python for building internal tooling for automation and monitoring with an in-house framework.

I've abused the language by creating my own variant of *literate Python* by using Python's AST module to compile code snippets embedded in Markdown.
Writing a blog post and want to reference a particular variable? It'd sure be helpful to visually trace that variable through code snippets, or to display an annotation box on hover with that variable's definition and the surrounding context.
> This led me to commit a small number of AST-based crimes that I'd love to talk more about someday. Maybe a future [blog post](/blog)?

{% end %}

{% skill(name="C") %}

While Rust has reasonable C interoperability, managing that boundary becomes difficult pretty quickly. That's often worth the effort, but sometimes you just need libc!

In C, I've written a number of small utilities to perform tasks that are frustrating to manage with Rust's libc API. Most recently, a utility to communicate filesystem changes to an websocket endpoint.

{% end %}

</fieldset>

<fieldset class="skill">
<legend align="right">Databases</legend>

{% skill(name="PostgreSQL") %}

PostgreSQL can do just about anything you'd ask of it. I won't claim to have covered every piece of ground, but I do regularly write complex queries with CTEs, materialized views, procedural PL/pgSQL, and other features. I'm no stranger to query optimization or table design.

{% end %}

{% skill(name="SQLite") %}

Not every project needs the powerhouse that is PostgreSQL just to store data. SQLite is powerful enough to trust, but small enough to reinvent if it disappeared tomorrow. [Growing use by companies like fly.io](https://fly.io/blog/all-in-on-sqlite-litestream/) gives me a lot of hope for SQLite's future.

{% end %}

{% skill(name="Graph Databases") %}

I'm going to be honest, I've never had an opportunity to use a graph database at work. I have enough experience using them for personal experiments that I'd be comfortable discussing challenges and victories I've had with them, but bringing these lessons to a production environment would be a new challenge.

{% end %}
</fieldset>

<fieldset class="skill">
<legend>Linux Administration</legend>

{% skill(name="Networking") %}

I'm won't claim to be a network engineer, but I'm happy to take charge and pick up the kernel's slack for reliable, high-performance kernel-bypass networking. I regularly deal with network programming and rudimentary packet capture analysis and I'm always happy to learn more.

{% end %}

{% skill(name="System Configuration" ) %}

Config is a deep, deep well. I'm currently hands-off in this role, but I've used [Ansible](https://www.ansible.com/) and [Rundeck](https://www.rundeck.com/) extensively at previous companies.

{% end %}

{% skill(name="Containers") %}

I've used Docker many times for personal projects, though my current role's focus on high-performance computing doesn't leave as much room for virtualization.

I have passing experience with [Firecracker](https://firecracker-microvm.github.io/), but I'm itching to deep-dive as soon as I'm out of school.

{% end %}

</fieldset>

<fieldset class="skill">
<legend>Computer Science</legend>

{% skill(name="Distributed Systems") %}

What's the rigorous theory behind guaranteeing reliability? How do autonomous agents agree on a collective truth? How can we design distributed systems that appear monolithic to users?

{% end %}

{% skill(name="Data Structures & Algorithms") %}

I don't spend my days grinding Leetcode, but knowing how B-Trees work has made me a better database steward and some algorithmic complexity fundamentals have let me help my colleagues in operations turn nested loops into constant-time lookups.

{% end %}

</fieldset>


</div>