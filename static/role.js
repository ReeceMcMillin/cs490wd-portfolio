let roleSpan = document.getElementById('role');
let roleList = [
    {"text": "student", "link": "education"},
    {"text": "site reliability engineer", "link": "work"},
    {"text": "composer", "link": "music"},
    {"text": "podcast editor", "link": "work"},
];

let currentIndex = 0;

const selectRole = idx => {
    role = roleList[idx];
    return role;
}

const changeRole = (direction) => {
    if (direction === 'init') {
        currentIndex = 0;
    } else if (direction === 'left') {
        currentIndex = (currentIndex - 1) % roleList.length;
        // Wrapping subtract to prevent negative indexing
        if (currentIndex === -1) currentIndex = roleList.length - 1;
    } else {
        currentIndex = (currentIndex + 1) % roleList.length;
    }
    console.log(currentIndex);
    let currentRole = selectRole(currentIndex);
    let roleLink = document.createElement('a');
    roleLink.href = currentRole.link;
    roleLink.innerText = currentRole.text;
    roleSpan.innerHTML = '';
    roleSpan.appendChild(roleLink);
}

window.addEventListener('load', function() {
    changeRole('init');
});

document.addEventListener('keydown', function(e) {
    if (e.key === 'ArrowLeft') {
        changeRole('left');
        document.getElementById('left-arrow').classList.add('hover');
    } else if (e.key === 'ArrowRight') {
        changeRole('right');
        document.getElementById('right-arrow').classList.add('hover');
    }
})

document.addEventListener('keyup', function(e) {
    if (e.key === 'ArrowLeft') {
        document.getElementById('left-arrow').classList.remove('hover');
    } else if (e.key === 'ArrowRight') {
        document.getElementById('right-arrow').classList.remove('hover');
    }
})