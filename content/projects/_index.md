+++
title = "Projects"
sort_by = "date"
template = "blog.html"
+++

# Right Now

Lately, I've been *deconstructing* the knowledge gained in my notetaking endeavors with the hopes of structuring them into a deep-dive YouTube series. I'm still working things out, but here's the idea:

<fieldset class="idea">
<legend>idea box</legend>

We'll build a static site generator from scratch, but the final product is more of a consequence. What I *really* want to do is explore the low-level mechanics of how static site generators work. We'll build things like:

- a markdown -> HTML compiler in Rust
- live-reload using websockets and C syscalls
- compile-time hierarchical reference management
- ... and whatever else comes up!

These ideas can feel big and hand-wavy until you've had a chance to reinvent them a time or two. That's what I'd like to facilitate next!

</fieldset>

# Notetaking

My favorite way to avoid studying is to write notetaking tools (you know, to help me study!). Frameworks like [Next.js](https://nextjs.org/) come with a lot of flexibility built in, but there's a special feeling in *compiling* your notes.

I recently wrote a static site generator in Rust that extends the popular Markdown parser [pulldown-cmark](https://github.com/raphlinus/pulldown-cmark) to support several unique features:

* Inline term definitions with hover-over references
* Compile-time warnings for orphan references
* LaTeX-to-SVG compilation via TiKZ at compile-time


<center>

![Video demonstration of the notetaking tool described above](md-notes.gif)

[The source is available on GitHub.](https://github.com/ReeceMcMillin/experimental-notes-engine)
</center>

# Iron March Geolocation

[Iron March](https://en.wikipedia.org/wiki/Iron_March) was a neo-Nazi web forum that operated from 2011-2017. It was a hotbed for radicalization and violence with several users having planned or committed hate crimes. The forum's database leaked in 2019 which led to a surge in independent counter-terrorism research. To contribute to that effort, I made a [web tool](https://reecemcmillin.github.io/ironmarch/) that mapped the IP address associated with user accounts to their geographic location.

<iframe src="https://reecemcmillin.github.io/ironmarch/" id="ironmarch">
iframe containing the Iron March geo-location tool
</iframe>

I continued this work by developing a Telegram archive utility called TeleScope to crawl neo-Nazi Telegram channels and preserve their chat logs for network analysis. I intended to publish this work, but managing an intense side project alongside school and work became difficult.

<center>

[An older version of TeleScope is archived on GitHub.](https://github.com/ReeceMcMillin/TeleScope)
</center>

# Whose Land

To learn basic web development using Python and Flask, I made a tool called [Whose Land](https://whose-land-concept.herokuapp.com/) that allows a user to search for an address and see the indigenous society that occupied that territory before American colonization.

<center>

![Video demonstration of the Whose Land tool described above](whoseland.gif)

</center>
