+++
title = "Education"
sort_by = "date"
template = "blog.html"
+++

<div class="left-justify">

My academic goals tend to revolve around distributed systems and type theory. Below is a list of courses I've taken at UMKC and the grades I received in each.

<span class="course-highlight">⭐ Starred courses</span> are those I've found prepared me especially well for my career in Site Reliability Engineering.

<fieldset class="semester">
<legend>Summer 2019</legend>

{% class(id="CONSVTY-103", name="Fundamentals of Music", credits=3, grade="A") %}
    An introduction to the basic elements of music and music notation, including the study of melody, rhythm, scales and keys, triads, 7th chords, the piano keyboard and the musical staff. Designed primarily for those interested in learning about music but with little or no formal study of music theory. 
{% end %}

{% class(id="ANCH-109", name="Education and Urban Society", credits=3, grade="A") %}
    This course is designed to introduce students to the social and philosophical issues in urban education and will include an emphasis on culture, race, class, and ethnicity as they relate to schooling in urban America. Students will engage in thinking and rethinking problems, issues and solutions that complicate our collective understanding of the intersection of urban society and education.
{% end %}

</fieldset>

<fieldset class="semester">
<legend>Fall 2019</legend>

{% class(id="CS-101", name="Problem Solving & Programming I", credits=3, grade="A") %}
    Problem solving, algorithms, and program design. Use of structured programming, lists, control structures, recursion, objects and files in Python. Introduction to graphical interface programming. Coding, testing and debugging using a modern development environment.
{% end %}

{% class(id="CS-101L", name="Problem Solving & Programming I - Lab", credits=1, grade="A") %}
    Programming exercises and demonstrations to reinforce concepts learned in CS101 and provide additional practice in Python programming.
{% end %}

{% class(id="POL-SCI 210", name="American Government", credits=3, grade="A") %}
    American government and politics, with special reference to the U.S. Constitution.  This course meets the state requirement for study of the U.S. and Missouri Constitutions. 
{% end %}

{% class(id="MATH-120", name="Precalculus", credits=5, grade="A") %}
    Functions and graphs. Inverses, compositions, and transformation of functions. Solving equations and  systems of equations, and inequalities. Linear, quadratic, polynomial, and rational functions. Exponential and logarithm functions and applications. Trigonometric functions, trigonometric identities, triangles.
{% end %}

</fieldset>

<fieldset class="semester">
<legend>Spring 2020</legend>

{% class(id="CS-191", name="Discrete Structures I", credits=3, grade="A") %}
    Mathematical logic, sets, relations, functions, mathematical induction, algebraic structures with emphasis on computing applications.
{% end %}

{% class(id="CS-201", name="Problem Solving & Programming II", credits=3, grade="A") %}	
Problem solving and programming using classes and objects. Algorithm efficiency, abstract data types, searching and sorting, templates, pointers, linked lists, stacks and queues implemented in C++.
{% end %}

{% class(id="CS-201L", name="Problem Solving & Programming II - Lab", credits=3, grade="A") %}
    	
Programming exercises and demonstrations to reinforce concepts learned in COMP-SCI 201R and provide additional practice in C++ programming. 
{% end %}


{% class(id="BIO-102", name="Biology and Living", credits=3, grade="A") %}
    Introduction to structural organization and functional processes of living systems. For non-biology majors only. Does not count toward biology degree. 
{% end %}

{% class(id="MATH-210", name="Calculus I", credits=4, grade="A") %} 	
    Functions and graphs, rational, trigonometric, exponential functions, composite and inverse functions, limits and continuity, differentiation and its applications, integration and its applications.
{% end %}

</fieldset>

<fieldset class="semester">
    <legend>Summer 2020</legend>
{% class(id="ANCH-209", name="World Cultures, Histories, and Ideas", credits=3, grade="A") %}
    This interdisciplinary course will explore the cultures, histories, and ideas of one or more regions of the world as well as dynamiCSof interaction between them. Students will be exposed to a very wide range of disciplinary approaches to this topic and learn how to engage critically in an interdisciplinary dialogue within this field. TopiCSwill vary depending on the instructors.
{% end %}

{% class(id="DISC-200", name="Discourse II", credits=3, grade="A") %}
    Students will produce, perform, and analyze college-level, oral and written texts that are based on sustained academic research. Students will continue to develop their understanding of discourse analysis and language awareness in the context of a range of discursive forms. Students will interpret and synthesize college-level scholarship that addresses how diverse discourse communities define, evaluate, and transform individual, institutional, and cultural identities.
{% end %}

</fieldset>

<fieldset class="semester">
    <legend>Fall 2020</legend>

{% class(id="PHYS-240", name="Physics for Scientists and Engineers I", credits=5, grade="A", hl=false) %}
    Introduction to mechanics, wave motion and sound and heat and thermodynamics. 
{% end %}

{% class(id="MATH-220", name="Calculus II", credits=4, grade="A") %}
Techniques of integration, applications of the definite integral, improper integrals, sequences and series, power series. Taylor series and convergence, analytic geometry in calculus.
{% end %}

{% class(id="CS-291", name="Discrete Structures II", credits=3, grade="A") %}
Recurrence relations and their use in the analysis of algorithms. Graphs, trees, and network flow models. Introduction to Finite state machines, grammars, and automata.
{% end %}


</fieldset>

<fieldset class="semester">
    <legend>Spring 2021</legend>

{% class(id="MATH-300", name="Linear Algebra I", credits=3, grade="A") %}
     	
This course introduces a branch of algebra developed from the theory of finding simultaneous solutions of a collection of linear equations, emphasizing fundamental concepts, calculations, and applications. TopiCSinclude matrix algebra, real vector spaces, linear transformations, determinants, the study of eigenvalues and eigenvectors, and the exploration of orthogonality and other applications.
{% end %}

{% class(id="MATH-250", name="Calculus III", credits=4, grade="A") %}

This is the third and final course of a standard three-semester calculus sequence. It covers solid analytic geometry, multiple-variable and vector-valued functions, partial derivatives, Lagrange multipliers, multiple integrals, Jacobians, line and surface integrals, Green’s Theorem, the Divergence Theorem, and Stokes’ Theorem. 
{% end %}

{% class(id="CS-281R", name="Computer Architecture and Organization", credits=3, grade="A", hl=true) %}
Digital Logic and Data Representation, process architecture and instruction sequencing, memory hierarchy and bus-interfaces and functional organization. 
{% end %}

{% class(id="CS-303", name="Data Structures", credits=3, grade="A") %}
Linear and hierarchical data structures, including stacks, queues, lists, trees, priority queues, advanced tree structures, hashing tables, dictionaries and disjoint-set. Abstractions and strategies for efficient implementations will be discussed. Linear and hierarchical algorithms will be studied as well as recursion and various searching and sorting algorithms. Programming concepts include Object Orientation, concurrency and parallel programming. Several in-depth projects in C++ will be required.
{% end %}




</fieldset>

<fieldset class="semester">
    <legend>Summer 2021</legend>

{% class(id="CS-491", name="Internship", credits=3, grade="CR", hl=true) %}
    Credit course for my internship at Cerner.
{% end %}

{% class(id="CS-394R", name="Applied Probability", credits=3, grade="A") %}
    Basic concepts of probability theory. Counting and measuring. Probability, conditional probability and independence. Discrete, continuous, joint random variables. Functions of random variables. Sums of independent random variables and transform methods. Random number generation and random event generation. Law of large numbers, central limit theorem, inequalities. Their applications to computer science and electrical and computer engineering areas are stressed.
{% end %}

</fieldset>

<fieldset class="semester">
    <legend>Fall 2021</legend>

{% class(id="DISC-300", name="Discourse III", credits=3, grade="A") %}
     	
Students will put the knowledge and skills learned in Discourse I and II into practical use by engaging in a service-learning project that is interdisciplinary and intercultural. Students will use strategies of critical discourse analysis and critical language awareness to target the appropriate audience/recipients for their research  project, to develop innovative and rhetorically effective texts, and to reflect on their project’s purpose, methods, and consequences.
{% end %}

{% class(id="CS-449", name="Foundations of Software Engineering", credits=3, grade="A") %}
     	
The course introduces concepts of software engineering (e.g. definitions, context) and the software development process (i.e. life cycle). Students will get a solid foundation in agile methodology, software requirements, exceptions and assertions, verification and validation, software models and modeling, and user Interface design. Various software architectures will be discussed.
{% end %}

{% class(id="MATH-301", name="Solid Ground: Sets & Proof", credits=3, grade="A") %}
     	
This course is a transition from procedural  mathematics,  such as  calculus, to advanced mathematics where proofs are the professional language of discourse.  It covers basic set theory and logic, relations and functions, and how to analyze, construct, and write clearly reasoned, well-structured elementary proofs using universal techniques.
{% end %}

{% class(id="CS-320", name="Data Communications & Networking", credits=3, grade="A", hl=true) %}
     	
This course examines the fundamental aspects of data communications and computer networks. Students will learn the concepts of layered models, communication protocols, digital transmission and encoding techniques, multiplexing techniques, error detection and correction concepts, the data link layer and its associated protocols, LAN architectures, switching techniques, major Internet protocols and standards, internetworking, addressing, and routing concepts. By the end of the course, students will be familiar with all networking concepts and technologies and will be able to size and implement different types of networking architectures. Part of the course will be devoted to the use of opensource software tools, such as Cisco Packet Tracer and Wireshark, to experiment with network architectures and analyze network traffic.

{% end %}

{% class(id="ANCH-308", name="Ethical Issues in Computing & Engineering", credits=3, grade="A") %}
     	
Societal and ethical obligations of computer science, IT, and electrical/computer engineering practice. TopiCSinclude ethical obligations of professional practice, electronic privacy, intellectual property, software and system security and reliability, and whistle-blowing. This course teaches the principles of ethical analysis and how technology, law, and ethiCSinteract in society, to help the graduate confront and deal with the ethical challenges that arise in professional practice.
{% end %}


</fieldset>

<fieldset class="semester">
    <legend>Spring 2022</legend>

{% class(id="CS-431", name="Intro to Operating Systems", credits=3, grade="A", hl=true) %}
     	
This course covers concurrency and control of asynchronous processes, deadlocks, memory management, processor and disk scheduling, x86 assembly language, parallel processing, security, protection, and file system organization in operating systems.
{% end %}

{% class(id="CS-470", name="Database Management Systems", credits=3, grade="A", hl=true) %}
     	
This course covers database architecture, data independence, schema, Entity-Relationship (ER) and relational database modeling, relational algebra and calculus, SQL, file organization, relational database design, physical database organization, query processing and optimization, transaction structure and execution, concurrency control mechanisms, database recovery, and database security.
{% end %}

{% class(id="CS-490", name="Distributed Computing Systems", credits=3, grade="A", hl=true) %}

This course will teach students the fundamentals of distributed computing systems that are the building blocks of large-scale software systems, such as cloud computing, search engines, and social networking platforms. The course will cover the following four groups of topics: distributed computing basics (e.g., system model, inter-process communication, messaging and remote invocations), distributed algorithms (e.g., time synchronization, distributed coordination algorithms, consensus), distributed services (e.g., distributed file systems, group communication, replication), and distributed systems (e.g., peer-to-peer systems, publish-subscribe systems).

{% end %}

{% class(id="CS-451", name="Software Engineering Capstone", credits=3, grade="A") %}
     	
	
The course will focus on the requirements and project planning and managing of medium sized projects with deliverables of each phase of the software life cycle. Additional studies of system integration and architecture, software modeling, requirements specifications, configuration management, verification, validation, software evolution and quality and finally measurement, estimation and economics of the software process.

{% end %}



</fieldset>

<fieldset class="semester">
    <legend>Summer 2022</legend>

{% class(id="CS-490WD", name="Web Development", credits=3, grade="IP") %}

An introduction to HTML, CSS, and JavaScript. This website is the capstone project for this course!

{% end %}



</fieldset>

<fieldset class="semester">
    <legend>Fall 2022</legend>

{% class(id="CS-404", name="Intro to Algorithms & Complexity", credits=3, grade="~") %}
     	
A rigorous review of asymptotic analysis techniques and algorithms: from design strategy (such as greedy, divide-and-conquer, and dynamic programming) to problem areas (such as searching, sorting, shortest path, spanning trees, transitive closures, and other graph algorithms, string algorithms) arriving at classical algorithms with supporting data structures for efficient implementation. Throughout, the asymptotic complexity is studied in worst case, best case, and average case for time and/or space, using appropriate analysis techniques (recurrence relations, amortization). Introduction to the basic concepts of complexity theory and NP-complete theory.
{% end %}

{% class(id="CS-441", name="Programming Languages: Design & Implementation", credits=3, grade="~") %}
    This course covers programming language paradigms (object-oriented programming, functional programming, declarative programming, and scripting) and design tradeoffs in terms of binding, visibility, scope, lifetime, type-checking, concurrency/parallelism, and abstraction. It also covers programming language specification, grammar, lexical analysis, exception handling, and runtime considerations. 
{% end %}

{% class(id="CS-461", name="Introduction to Artificial Intelligence", credits=3, grade="~") %}
    This course provides an overview of the field of artificial intelligence. Topics include guided and unguided search, adversarial search, generation and use of heuristics, logic programming, probabilistic reasoning, and neural networks. Application areas studied include game playing, automated proofs, expert systems, and data mining. 
{% end %}

{% class(id="PHYS-250", name="Physics for Scientists & Engineers II", credits=5, grade="~") %}
    Introduction to electricity and magnetism, light and optics and modern physics. Four hours lecture and two hours laboratory per week. 
{% end %}

</fieldset>
</div>

<fieldset class="semester">
<legend>Spring 2023</legend>

{% class(id="CS-49x", name="Elective/Independent Study", credits=3, grade="~") %}
    Directed readings in distributed systems. Content yet to be determined.
{% end %}

</fieldset>
</div>