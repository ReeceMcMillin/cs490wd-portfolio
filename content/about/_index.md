+++
title = "About"
template = "blog.html"
+++
<!-- 
<div class="headshot-container">
    <img src="../headshot-square.jpg" id="headshot"/>
</div> -->

Hey, I'm Reece! I'm a Kansas native, student, and Site Reliability Engineer. That last part is new, but I've been doing things for a while!

<div id="separator">~</div>

I graduated high school in 2012 (just barely). Traditional higher education didn't seem promising, so I chose to study audio engineering at [The Conservatory of Recording Arts and Sciences](https://www.cras.edu/) in the suburbs of Phoenix, AZ. This gave me the opportunity to move to New York City in 2013 to work unpaid with Grammy-nominated artists at the now-defunct Brooklyn recording studio, The Gallery.

Only months later, a friend told me he'd be opening a studio of his own in Connecticut and wanted me to lead music recording efforts as co-owner. My 19-year-old sense of invincibility led me to a small building nestled a few feet from the Connecticut River where we spent months constructing a small live-in recording studio. After two years of excellent music and solidly negative ROI, I'd exhausted my body, my mental health, and my bank account. I sold my half of the company and moved back to my hometown.

I spent the next few months graciously taking up space with family while working for the local coffee shop & roasterie. I stayed deeply involved in the music industry, writing a number of (surprisingly successful) songs as [Aerocity](https://aerocity.bandcamp.com/). It was around this time that experimental work by [Ólafur Arnalds](https://soundcloud.com/olafur-arnalds/tree?in=nils_frahm/sets/piano-day-playlist-march-29th) led me to develop an interest in machine learning. All it took was watching a few pop-math YouTube videos - I enrolled at Johnson County Community College to with some vague idea of studying "mathematics" or "computer science" (whatever that meant). I found a job at Starbucks and moved to the Kansas City area that month.

While studying at JCCC, my ever-bouncing brain discovered a love for the coffee industry. I wanted to go deeper - I took a short break from school to become the <span class="subtle">[*cough - award-winning*]</span> head coffee roaster at [The Roasterie](https://theroasterie.com/). This short break turned out to be not as short as I had expected, and but I did eventually find my way back towards academia via the Computer Science B.S. program at the University of Missouri - Kansas City.

I developed a real passion for things like graph theory, complexity, and distributed systems. That passion led me to spend a year as an apprentice with a phenomenal DevOps team at [Cerner](https://www.cerner.com/) before landing in my current position as an intern with the Site Reliability Engineering team at [Cboe Global Markets](https://www.cboe.com/).

You can read more about my [work](../work/), [education](../education/), or fun half-baked ideas on my [blog](../blog/).